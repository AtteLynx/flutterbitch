* All code should be formatted with `cargo fmt`.
* All code should pass `cargo clippy` without any errors or warnings.